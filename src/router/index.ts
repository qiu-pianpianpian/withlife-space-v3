import { createRouter, createWebHashHistory } from 'vue-router'
import PianPian from  '@/views/pianpian/index.vue'
import TaoTao from '@/views/taotao/index.vue'
const routes = [
  {
    path: '/',
    component: PianPian
  },
  { 
    path: '/taotao',
    component: TaoTao
  },
]
export const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
  scrollBehavior: () => ({ left: 0, top: 0 })
})