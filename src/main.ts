import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './style.css'
import App from './App.vue'
import pinia from '@/stores'
import { router } from '@/router/index'
import '@unocss/reset/tailwind.css'
import './style.css'
import 'uno.css'
// createApp(App).mount('#app')
const app = createApp(App)
app.use(pinia)
app.use(router)
app.use(ElementPlus)
app.mount('#app')