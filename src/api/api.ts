// 1、增删改查后能够实时看到效果
// 2、表头点击日期会有升序倒序功能
// 3、新增修改共用同一个对话框，以表单形式提交修改
// 4、删除前要弹出是否确认删除

export interface DataModel {
  id?: number
  avatar: string // 头像url
  name: string // 用户名称
  phone: number // 用户手机号
  province: string // 用户住址省份
  city: string // 用户住址城市
  county: string // 用户住址区县
  address: string // 用户住址详细地址
  code: number // 用户所在邮编，限6位数
  createTime: number // 创建时间戳
}
interface MessageModel<T = DataModel> {
  code: number
  msg: string
  data?: T
}
const data: DataModel[] = [
  {
    id: 1,
    avatar: 'http://files.jiangtao.ltd/10000.jpg',
    name: '江涛',
    phone: 18899796648,
    province: '广东',
    city: '广州',
    county: '白云区',
    address: '住在叽里呱啦最喜欢小翩的地方',
    code: 200333,
    createTime: 1605232905000,
  },
  {
    id: 2,
    avatar: 'http://files.jiangtao.ltd/10000.jpg',
    name: '小翩',
    phone: 18899796648,
    province: '湖南',
    city: '长沙',
    county: '芙蓉区',
    address: '住在叽里呱啦最喜欢小涛老公的地方',
    code: 500100,
    createTime: 1605232666666,
  },
  {
    id: 3,
    avatar: 'http://files.jiangtao.ltd/10043.png',
    name: '某某人1',
    phone: 13044244002,
    province: '广东',
    city: '广州',
    county: '白云区',
    address: '住在叽里呱啦最喜欢小翩的地方',
    code: 123456,
    createTime: 1605232888888,
  },
  {
    id: 4,
    avatar: 'http://files.jiangtao.ltd/10044.png',
    name: '某某人2',
    phone: 13044244002,
    province: '广东',
    city: '广州',
    county: '白云区',
    address: '住在叽里呱啦最喜欢小翩的地方',
    code: 123456,
    createTime: 1605232123458,
  },
  {
    id: 5,
    avatar: 'http://files.jiangtao.ltd/10045.png',
    name: '某某人3',
    phone: 13044244002,
    province: '广东',
    city: '广州',
    county: '白云区',
    address: '住在叽里呱啦最喜欢小翩的地方',
    code: 123456,
    createTime: 1605232456789,
  },
]

// 获取数据
export function getData() {
  return new Promise<MessageModel<DataModel[]>>((resolve) => {
    const datas: MessageModel<DataModel[]> = {
      code: 0,
      msg: '获取成功',
      data,
    }
    resolve(datas)
  })
}

// 修改或增加数据
export function modifyData(sendData: DataModel) {
  return new Promise<MessageModel>((resolve) => {
    let datas: MessageModel
    if (!sendData.id) { // 新增
      data.push(sendData)
      datas = {
        code: 0,
        msg: '新增成功',
      }
    }
    else { // 修改
      const index = data.findIndex(item => item.id === sendData.id)
      data[index] = sendData
      datas = {
        code: 0,
        msg: '修改成功',
      }
    }
    resolve(datas)
  })
}
// 删除数据
export function deleteData({ id }: { id: number }) {
  return new Promise<MessageModel>((resolve) => {
    let datas: MessageModel
    if (!id) {
      datas = {
        code: -1,
        msg: '参数错误',
      }
      resolve(datas)
    }
    else {
      const index = data.findIndex(item => item.id === id)
      data.splice(index, 1)
      datas = {
        code: 0,
        msg: '删除成功',
      }
      resolve(datas)
    }
  })
}
