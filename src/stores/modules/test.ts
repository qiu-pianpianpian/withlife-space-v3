import { defineStore } from 'pinia'

interface State {
  age: number
}

export const useTestStore = defineStore('test', {
  state: (): State => {
    return {
      age: 0
    }
  },
  getters: {
  },
  actions: {
  },
})