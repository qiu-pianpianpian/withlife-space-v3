import { defineStore } from 'pinia'
import { useTestStore } from "@/stores/modules/test";

interface State {
  count: number
  name: any[]
}

export const useUserStore = defineStore('user', {
  state: (): State => {
    return {
      count: 0,
      name: []
    }
  },
  getters: {
    double: (state) => state.count * 2,
    doubleFn(state) {
      return (count) => state.count = count
    }
  },
  actions: {
    increment(): Promise<number> {
      this.count++
      return Promise.resolve(0)
    },
    modifyAll(num: number) {
      const useTestStoreFn = useTestStore()
      this.count = num
      useTestStoreFn.age = num
    }
  },
})