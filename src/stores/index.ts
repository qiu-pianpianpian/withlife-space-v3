import 'pinia'

declare module 'pinia' {
  export interface PiniaCustomProperties {
    // 你也可以定义更简单的值
    simpleNumber: number
  }
}

import { createPinia } from 'pinia'
import type { PiniaPluginContext } from 'pinia'

const pinia = createPinia()

function taotao(content: PiniaPluginContext) {
  console.log(content, 'contentcontentcontentcontent')
  return {
    simpleNumber: 1
  }
}

pinia.use(taotao)
export default pinia